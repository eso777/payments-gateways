<?php

namespace App\Services;

use App\Product;
use App\Http\Controllers\Controller;

class ProductServices extends Controller
{
    /**
     * @param int $perPage
     * @return mixed
     */
    public function getProducts($perPage = 20)
    {
        return Product::paginate($perPage);
    }

    /**
     * @uses GET PRODUCT BY ID
     * @param $id
     *
     * @return mixed
     */
    public function getProductById($id)
    {
        if (! $product = Product::find($id)) {
            abort('404');
        }

        return $product;
    }
}
