<?php

namespace App\Http\Controllers;

use App\Services\ProductServices;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $productServices;

    /**
     * ProductController constructor.
     * @param ProductServices $productServices
     */
    public function __construct(ProductServices $productServices)
    {
        $this->productServices = $productServices;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $products = $this->productServices->getProducts();
        return view('home', compact('products'));
    }

    public function show($id)
    {
        $product = $this->productServices->getProductById($id);
        return view('productDetails', compact('product'));
    }
}
