@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="content">
                <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
                      rel="stylesheet" id="bootstrap-css">
                <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                <!------ Include the above in your HEAD tag ---------->
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
                <div class="container">
                    <h3 class="h3"> H & M </h3>
                    <div class="row">
                        @if($products->total())
                            @foreach($products as $product)
                                <div class="col-md-3 col-sm-6">
                                    <div class="product-grid3">
                                        <div class="product-image3">
                                            <a href="#">
                                                <img class="pic-2"
                                                src="http://bestjquery.com/tutorial/product-grid/demo4/images/img-2.jpg">
                                                <img class="pic-1"
                                                     src="http://bestjquery.com/tutorial/product-grid/demo4/images/img-1.jpg">
                                            </a>
                                            <ul class="social">
                                                <li><a href="#"><i class="fa fa-shopping-bag"></i></a></li>
                                                <li><a href="{{route('show', $product->id)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                            <span class="product-new-label">New</span>
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a href="#">{{ $product->name }}</a></h3>
                                            <div class="price">
                                                {{$product->price}}
                                                <span>$ {{ ($product->price) + $product->price * 10/100 }}</span>
                                            </div>
                                            <ul class="rating">
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star"></li>
                                                <li class="fa fa-star disable"></li>
                                                <li class="fa fa-star disable"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h3> No Data To Show !</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
