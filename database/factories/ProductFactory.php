<?php

use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {

    \App\Product::truncate();
    return [
        'product_name' => $faker->name,
        'price'        => ($faker->numberBetween(1000, 10000)),
        'image'        => $faker->imageUrl('150', '150', 'fashion'),
        'description'  => $faker->text,
    ];
});
